package pl.polsl.cars.neural;

import java.util.ArrayList;
import java.util.List;

public class NeuralNetwork {

    private List<Node> hiddenLayer1;
    private final int hiddenLayer1Size;
    private List<Node> outputLayer;
    private final int outputLayerSize;
    private int inputLayerSize;

    public NeuralNetwork() {
        hiddenLayer1 = new ArrayList<>();
        hiddenLayer1Size = 4;
        outputLayer = new ArrayList<>();
        outputLayerSize = 2;
    }

    public float getBestEngineValue() {
        return outputLayer.get(0).getActivationValue();
    }

    public float getBestTurnValue() {
        return outputLayer.get(1).getActivationValue();
    }

    public void calculate(float[] inputValues, float[] genome) {
        inputLayerSize = inputValues.length;
        calculateFirstHiddenLayer(inputValues, genome);
        calculateOutputLayer(genome);
    }

    private void calculateFirstHiddenLayer(float[] inputValues, float[] genome) {
        hiddenLayer1 = new ArrayList<>();
        for (int i = 0; i < hiddenLayer1Size; i++) {
            float value = 0f;
            for (int j = 0; j < inputValues.length; j++) {
                value += inputValues[j]*genome[hiddenLayer1Size * j + i];
            }
            Node node = new Node(value, sigmoid(value));
            hiddenLayer1.add(node);
        }
    }

    private void calculateOutputLayer(float[] genome) {
        outputLayer = new ArrayList<>();
        int previousGenes = inputLayerSize * hiddenLayer1Size;
        for (int i = 0; i < outputLayerSize; i++) {
            float value = 0f;
            for (int j = 0; j < hiddenLayer1Size; j++) {
                value += hiddenLayer1.get(j).getActivationValue()*genome[previousGenes + outputLayerSize * j + i];
            }
            Node node = new Node(value, value);
            outputLayer.add(node);
        }
    }

    private float sigmoid(float x) {
        return (float)(1 / (1 + Math.exp(-x)));
    }
}
