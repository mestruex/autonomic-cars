package pl.polsl.cars.neural;

public class Node {

    private float value;
    private float activationValue;

    public Node() {
    }

    public Node(float value, float activationValue) {
        this.value = value;
        this.activationValue = activationValue;
    }

    public float getValue() {
        return value;
    }

    public Node setValue(float value) {
        this.value = value;
        return this;
    }

    public float getActivationValue() {
        return activationValue;
    }

    public Node setActivationValue(float activationValue) {
        this.activationValue = activationValue;
        return this;
    }
}
