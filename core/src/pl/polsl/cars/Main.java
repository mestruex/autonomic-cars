package pl.polsl.cars;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import pl.polsl.cars.genetic.FitnessFunction;
import pl.polsl.cars.genetic.GeneticAlgorithm;
import pl.polsl.cars.model.Car;
import pl.polsl.cars.model.Position;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

public class Main extends ApplicationAdapter {

	private static final float lerp = 8f;

	private FitnessFunction fitnessFunction;
	private GeneticAlgorithm geneticAlgorithm;

	private SpriteBatch batch;
	private OrthographicCamera camera;
	private Viewport viewport;

	private TiledMap map;
	private TiledMapRenderer tiledMapRenderer;
	private MapObjects collisionLayer;
	private MapObjects checkpointLayer;

	private ShapeRenderer shapeRenderer;
	private int i = 0;
    private ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(12);
    private List<Callable<Boolean>> callables = new ArrayList<>();

    private int deadCars = 0;
    private float highestFitness = 0;
    private Car fittestCar;

	@Override
	public void create () {
        initGraphics();

        final String settingsFile = "settings.txt";
        String mapName = null, genome = null;
        try (Scanner s = new Scanner(new File(settingsFile))) {
            System.out.println("Loading settings");
            if (s.hasNextLine()) {
                mapName = s.nextLine() + ".tmx";
                System.out.println("Map: " + mapName);
            }
            if (s.hasNextLine()) {
                genome = s.nextLine();
                System.out.println("Genome: " + genome);
            }
        } catch (IOException e) {
            System.out.println("settings.txt is invalid or missing, loading defaults...");
        }
        if (mapName == null) {
            System.out.println("Map not chosen, loading default map");
            mapName = "map2.tmx";
        }

        map = new TmxMapLoader().load(mapName);
        tiledMapRenderer = new OrthogonalTiledMapRenderer(map, batch);

        final int x, y;
        if (map.getLayers().get(0).getName().equals("map1")) {
            x = 100;
            y = 1120;
        } else {
            x = 220;
            y = 1180;
        }

        List<Position> checkpoints = loadCheckpointsFromMap(map);

        fitnessFunction = new FitnessFunction(checkpoints);
        if (genome == null) {
            System.out.println("Genome not set, generating random genome");
            geneticAlgorithm = new GeneticAlgorithm(x, y, fitnessFunction, 250, 32);
        } else {
            geneticAlgorithm = new GeneticAlgorithm(x, y, fitnessFunction, genome);
        }

		final InputHandler inputHandler = new InputHandler();
		Gdx.input.setInputProcessor(inputHandler);
	}

    private void initGraphics() {
        shapeRenderer = new ShapeRenderer();
        batch = new SpriteBatch();
        camera = new OrthographicCamera();
        viewport = new FillViewport(640, 640, camera);
        viewport.apply();

        camera.position.set(camera.viewportWidth / 2, camera.viewportHeight / 2, 0);
        camera.setToOrtho(false);
    }

    private List<Position> loadCheckpointsFromMap(TiledMap map) {
        collisionLayer = map.getLayers().get("collision").getObjects();
        checkpointLayer = map.getLayers().get("checkpoints").getObjects();

        List<Position> checkpoints = new ArrayList<>(checkpointLayer.getCount());
        for (int i = 0; i < checkpointLayer.getCount(); i++) {
            checkpoints.add(new Position(0, 0));
        }
        for (MapObject mapObject : checkpointLayer) {
            checkpoints.set(Integer.parseInt(mapObject.getName()),
                new Position(mapObject.getProperties().get("x", Float.class), mapObject.getProperties().get("y", Float.class)));
        }
        return checkpoints;
    }

    private void update() {
		for (Car car : geneticAlgorithm.getPopulation()) {
		    // each car is updated on different thread so it's faster
		    callables.add(() -> {
                if (!car.isDead()) {
                    updateCarSensorValues(car);

                    car.getBrain().calculate(car.getSensors(), car.getGenome());
                    car.setEngine(car.getBrain().getBestEngineValue());
                    // accelerating turns, so they are more dynamic
                    car.setTurn(car.getBrain().getBestTurnValue() * -9f);

                    updateCarVelocity(car);
                    updateCarPosition(car);

                    checkIfCarCollidedWithWall(car);
                    checkIfReachedNextCheckpoint(car);
                } else {
                    deadCars++;
                }
                return true;
            });
		}

        try {
            executor.invokeAll(callables);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        callables.clear();

		if (deadCars >= geneticAlgorithm.getPopulation().size()) {
		    highestFitness = 0;
		    geneticAlgorithm.evolvePopulation();
        }
        deadCars = 0;
    }

    private void checkIfReachedNextCheckpoint(Car car) {
	    if (Intersector.overlapConvexPolygons(((PolygonMapObject)checkpointLayer.get(car.getPassedCheckpoints())).getPolygon(), car.getPolygon())) {
	        car.setPassedCheckpoints(car.getPassedCheckpoints() + 1);
        }
    }

    private void updateCarSensorValues(Car car) {
        car.setSensors(new float[]{1f, 1f, 1f, 1f, 1f, car.getVelocity()});
        boolean[] sensorCollided = new boolean[]{false, false, false, false, false};
        List<Polygon> sensorPolygons = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            sensorPolygons.add(getSensorPolygon(car, i));
        }
        // sensor collision works by casting a ray in each direction, when ray collides, the distance is saved as a sensor value
        for (int i = 1; i <= 90; i++) {
            // 5 sensors
            for (int j = 0; j < 5; j++) {
                // cast ray further
                sensorPolygons.get(j).setScale(1f, 1f + 0.3f * i);
                for (final PolygonMapObject polygon : collisionLayer.getByType(PolygonMapObject.class)) {
                    if (!sensorCollided[j] && Intersector.overlapConvexPolygons(polygon.getPolygon(), sensorPolygons.get(j))) {
                        // save ray collision distance
                        car.getSensors()[j] = 0.0f + i/100f;
                        sensorCollided[j] = true;
                    }
                }
                // set ray length to the beginning
                sensorPolygons.get(j).setScale(1f, 1f/(1f + 0.3f * i));
            }
        }
    }

    private void checkIfCarCollidedWithWall(Car car) {
        for (final PolygonMapObject polygon : collisionLayer.getByType(PolygonMapObject.class)) {
            if (Intersector.overlapConvexPolygons(polygon.getPolygon(), car.getPolygon())) {
                car.setDead(true);
            }
        }
    }

    private void updateCarPosition(Car car) {
        float newX = (float)(car.getPosition().getX() + Math.cos(car.getTurn() + Math.PI / 2f) * car.getVelocity());
        float newY = (float)(car.getPosition().getY() + Math.sin(car.getTurn() + Math.PI / 2f) * car.getVelocity());
        car.setPosition(new Position(newX, newY));

        car.getSprite().setPosition(car.getPosition().getX(), car.getPosition().getY());
        car.getSprite().setRotation(car.getTurn() * MathUtils.radiansToDegrees);
        car.getPolygon().setPosition(car.getPosition().getX(), car.getPosition().getY());
        car.getPolygon().setRotation(car.getTurn() * MathUtils.radiansToDegrees);
    }

    private void updateCarVelocity(Car car) {
        car.setVelocity(car.getVelocity() + car.getEngine());
        // limit top and min velocity
        if (car.getVelocity() > 10f) {
            car.setVelocity(10f);
        } else if (car.getVelocity() < -10f) {
            car.setVelocity(-10f);
        } else if (car.getVelocity() > 0f && car.getVelocity() < 1f) {
            car.setVelocity(1f);
        } else if (car.getVelocity() < 0f && car.getVelocity() > -1f) {
            car.setVelocity(-1f);
        }
    }

    @Override
	public void render () {
		update();
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        for (Car car : geneticAlgorithm.getPopulation()) {
            float fitness = fitnessFunction.calculateFitness(car);
            if (fitness > highestFitness) {
                highestFitness = fitness;
                fittestCar = car;
                i = 0;
            }
        }
        i++;

        // if fittest car didn't change for 450/60 = 7.5sec, then kill whole population
        // someone is either stuck or too slow to be considered
        if (i == 450) {
            geneticAlgorithm.getPopulation().forEach(car -> car.setDead(true));
            i = 0;
        }

        // follow car with a camera
		followCar(fittestCar == null ? geneticAlgorithm.getPopulation().get(0) : fittestCar, Gdx.graphics.getDeltaTime());

		camera.update();
		tiledMapRenderer.setView(camera);
		tiledMapRenderer.render();

		batch.begin();
		for (Car car : geneticAlgorithm.getPopulation()) {
			car.getSprite().draw(batch);
		}

		// showing debug lines can be disabled by clicking "o"
		if (InputHandler.OBJECT_LAYER_DEBUG) {
            renderDebugLines();
        }
		batch.end();
	}

    private void renderDebugLines() {
        for (Car car : geneticAlgorithm.getPopulation()) {
            shapeRenderer.setProjectionMatrix(camera.combined);
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(Color.GREEN);
            shapeRenderer.polygon(car.getPolygon().getTransformedVertices());
            shapeRenderer.end();
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
            shapeRenderer.setColor(Color.PURPLE);
            shapeRenderer.point(car.getPosition().getX() + Car.WIDTH/2f, car.getPosition().getY() + Car.LENGTH/2f, 1);
            shapeRenderer.end();
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(Color.YELLOW);
            Polygon rPoly0 = getSensorPolygon(car, 0);
            rPoly0.setScale(1f, 30f);
            Polygon rPoly1 = getSensorPolygon(car, 1);
            rPoly1.setScale(1f, 30f);
            Polygon rPoly2 = getSensorPolygon(car, 2);
            rPoly2.setScale(1f, 30f);
            Polygon rPoly3 = getSensorPolygon(car, 3);
            rPoly3.setScale(1f, 30f);
            Polygon rPoly4 = getSensorPolygon(car, 4);
            rPoly4.setScale(1f, 30f);

            shapeRenderer.polygon(rPoly0.getTransformedVertices());
            shapeRenderer.polygon(rPoly1.getTransformedVertices());
            shapeRenderer.polygon(rPoly2.getTransformedVertices());
            shapeRenderer.polygon(rPoly3.getTransformedVertices());
            shapeRenderer.polygon(rPoly4.getTransformedVertices());
            shapeRenderer.end();
        }

        for (final PolygonMapObject polygon : collisionLayer.getByType(PolygonMapObject.class)) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(Color.RED);
            shapeRenderer.polygon(polygon.getPolygon().getTransformedVertices());
            shapeRenderer.end();
        }

        for (final PolygonMapObject polygon : checkpointLayer.getByType(PolygonMapObject.class)) {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.setColor(Color.BLUE);
            shapeRenderer.polygon(polygon.getPolygon().getTransformedVertices());
            shapeRenderer.end();
        }
    }

    private Polygon getSensorPolygon(Car car, int sensorNumber) {
        Rectangle rect = new Rectangle(car.getPolygon().getX() + Car.WIDTH/2f, car.getPolygon().getY() + Car.LENGTH/2f, 0.2f, 10f);
        Polygon rPoly = new Polygon(new float[] {
            rect.x, rect.y,
            rect.x, rect.y + rect.height,
            rect.x + rect.width, rect.y + rect.height,
            rect.x + rect.width, rect.y
        });
        rPoly.setOrigin(rect.x, rect.y);
        rPoly.rotate(car.getPolygon().getRotation() - 90f + sensorNumber * 45f);
        return rPoly;
    }

    @Override
	public void resize(int width, int height) {
		viewport.update(width, height);
	}

	@Override
	public void dispose () {
		batch.dispose();
	}

	private void followCar(final Car car, final float delta) {
		viewport.getCamera().translate((car.getPosition().getX() - viewport.getCamera().position.x) * lerp * delta,
			(car.getPosition().getY() - viewport.getCamera().position.y) * lerp * delta, 0);

		final float halfOfViewportWidth = camera.viewportWidth / 2;
		final float halfOfViewportHeight = camera.viewportHeight / 2;

		if (viewport.getCamera().position.x < halfOfViewportWidth) {
			viewport.getCamera().position.x = halfOfViewportWidth;
		}

		if (viewport.getCamera().position.y < halfOfViewportHeight) {
			viewport.getCamera().position.y = halfOfViewportHeight;
		}

		if (viewport.getCamera().position.x > 1280 - halfOfViewportWidth) {
			viewport.getCamera().position.x = 1280 - halfOfViewportWidth;
		}

		if (viewport.getCamera().position.y > 1280 - halfOfViewportHeight) {
			viewport.getCamera().position.y = 1280 - halfOfViewportHeight;
		}
	}
}
