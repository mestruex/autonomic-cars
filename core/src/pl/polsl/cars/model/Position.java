package pl.polsl.cars.model;

public class Position {

    private float x;
    private float y;

    public Position(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public Position setX(float x) {
        this.x = x;
        return this;
    }

    public float getY() {
        return y;
    }

    public Position setY(float y) {
        this.y = y;
        return this;
    }
}
