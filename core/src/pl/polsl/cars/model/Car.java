package pl.polsl.cars.model;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import pl.polsl.cars.neural.NeuralNetwork;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Car {

    public static final int WIDTH = 20;
    public static final int LENGTH = 35;

    private Position position;
    private Sprite sprite;
    private Polygon polygon;

    private float velocity;
    private float engine;
    private float turn;
    private int passedCheckpoints;

    private boolean dead;
    private boolean genomePrinted;

    // 0 - close right, 1 - far right, 2 - front, 3 - far left, 4 - close left, 5 - currentVelocity
    // values: 0 = touching, 1 = nothing in front of a sensor
    private float[] sensors;

    // genome consists of weights between nodes. Values range from -1 to 1
    private float[] genome;

    private NeuralNetwork brain;

    public Car(Position position) {
        this.genomePrinted = false;
        this.position = position;
        this.velocity = 0;
        this.engine = 0;
        this.turn = -(float)Math.PI/2f;
        this.sensors = new float[6];
        this.passedCheckpoints = 1;
        this.brain = new NeuralNetwork();
        // graphics
        this.sprite = new Sprite(new Texture("car.png"));
        this.sprite.setPosition(position.getX(), position.getY());
        this.sprite.setRotation(this.turn * MathUtils.radiansToDegrees);
        // collision
        this.polygon = new Polygon(new float[] {0, 0, WIDTH, 0, WIDTH, LENGTH, 0, LENGTH});
        this.polygon.setOrigin(WIDTH/2f, LENGTH/2f);
        this.polygon.setPosition(position.getX(), position.getY());
        this.polygon.setRotation(this.turn * MathUtils.radiansToDegrees);
    }

    public Car(Position position, int numberOfGenes) {
        this(position);
        this.genome = generateRandomGenome(numberOfGenes);
    }

    public Car(Position position, float[] genome) {
        this(position);
        this.genome = genome;
    }

    public Car(Position position, String genome) {
        this(position);
        this.genome = parseStringGenome(genome);
    }

    private float[] generateRandomGenome(int numberOfGenes) {
        float[] newGenome = new float[numberOfGenes];
        Random rand = new Random();

        for (int i = 0; i < numberOfGenes; i++) {
            newGenome[i] = rand.nextFloat();
            if (rand.nextBoolean()) {
                newGenome[i] = -newGenome[i];
            }
        }

        return newGenome;
    }

    private float[] parseStringGenome(String genome) {
        List<Float> geneList = new ArrayList<>();
        for (String gene : genome.split(",")) {
            geneList.add(Float.parseFloat(gene));
        }
        float[] parsedGenome = new float[geneList.size()];
        for (int i = 0; i < geneList.size(); i++) {
            parsedGenome[i] = geneList.get(i);
        }
        return parsedGenome;
    }

    public Position getPosition() {
        return position;
    }

    public Car setPosition(Position position) {
        this.position = position;
        return this;
    }

    public float getVelocity() {
        return velocity;
    }

    public Car setVelocity(float velocity) {
        this.velocity = velocity;
        return this;
    }

    public float getEngine() {
        return engine;
    }

    public Car setEngine(float engine) {
        this.engine = engine;
        return this;
    }

    public float getTurn() {
        return turn;
    }

    public Car setTurn(float turn) {
        this.turn = turn;
        return this;
    }

    public float[] getSensors() {
        return sensors;
    }

    public Car setSensors(float[] sensors) {
        this.sensors = sensors;
        return this;
    }

    public float[] getGenome() {
        return genome;
    }

    public Car setGenome(float[] genome) {
        this.genome = genome;
        return this;
    }

    public int getPassedCheckpoints() {
        return passedCheckpoints;
    }

    public Car setPassedCheckpoints(int passedCheckpoints) {
        this.passedCheckpoints = passedCheckpoints;
        return this;
    }

    public boolean isDead() {
        return dead;
    }

    public Car setDead(boolean dead) {
        this.dead = dead;
        return this;
    }

    public boolean isGenomePrinted() {
        return genomePrinted;
    }

    public Car setGenomePrinted(boolean genomePrinted) {
        this.genomePrinted = genomePrinted;
        return this;
    }

    public NeuralNetwork getBrain() {
        return brain;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public Car setSprite(Sprite sprite) {
        this.sprite = sprite;
        this.sprite.setRotation(this.turn * MathUtils.radiansToDegrees);
        this.sprite.setPosition(this.position.getX(), this.position.getY());
        this.polygon = new Polygon(new float[] {0, 0, WIDTH, 0, WIDTH, LENGTH, 0, LENGTH});
        this.polygon.setOrigin(WIDTH/2f, LENGTH/2f);
        this.polygon.setPosition(position.getX(), position.getY());
        this.polygon.setRotation(this.turn * MathUtils.radiansToDegrees);
        return this;
    }

    public Polygon getPolygon() {
        return polygon;
    }

    public Car setPolygon(Polygon polygon) {
        this.polygon = polygon;
        return this;
    }

    public void printReadableGenome() {
        DecimalFormat formatter = new DecimalFormat("#.##");
        StringBuilder sb = new StringBuilder();
        for (float gene : genome) {
            sb.append(formatter.format(gene)).append(",");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        System.out.println(sb.toString());
    }

    public void printGenome() {
        StringBuilder sb = new StringBuilder();
        for (float gene : genome) {
            sb.append(gene).append(",");
        }
        sb.deleteCharAt(sb.lastIndexOf(","));
        System.out.println(sb.toString());
    }
}
