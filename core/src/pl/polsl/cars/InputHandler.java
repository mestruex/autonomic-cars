package pl.polsl.cars;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import pl.polsl.cars.model.Car;

public class InputHandler implements InputProcessor {

    static boolean OBJECT_LAYER_DEBUG = true;

    InputHandler() {
    }

    @Override
    public boolean keyDown(int keycode) {

        if (keycode == Input.Keys.O) {
            OBJECT_LAYER_DEBUG = !OBJECT_LAYER_DEBUG;
        }

        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
