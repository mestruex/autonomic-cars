package pl.polsl.cars.genetic;

import pl.polsl.cars.model.Car;
import pl.polsl.cars.model.Position;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class GeneticAlgorithm {

    private final int startX;
    private final int startY;

    private static final int desiredPopSize = 250;
    // chance for crossing a single gene
    private final float uniformRate = 0.5f;

    // chance for mutating a single gene
    private float mutationRate = 0.025f;

    private int currentGeneration;
    private List<Car> population;
    private FitnessFunction fitnessFunction;

    private GeneticAlgorithm(int startX, int startY, FitnessFunction fitnessFunction, int popSize) {
        this.startX = startX;
        this.startY = startY;
        this.fitnessFunction = fitnessFunction;
        this.currentGeneration = 1;
        this.population = new ArrayList<>(popSize);
    }

    public GeneticAlgorithm(int startX, int startY, FitnessFunction fitnessFunction, int popSize, int genomeSize) {
        this(startX, startY, fitnessFunction, popSize);
        for (int i = 0; i < popSize; i++) {
            population.add(new Car(new Position(startX, startY), genomeSize));
        }
    }

    public GeneticAlgorithm(int startX, int startY, FitnessFunction fitnessFunction, String genome) {
        this(startX, startY, fitnessFunction, 3);
        for (int i = 0; i < 3; i++) {
            population.add(new Car(new Position(startX, startY), genome));
        }
    }

    public List<Car> evolvePopulation() {
        List<Car> parents = getBestParents(population);
        List<Car> children = new ArrayList<>();
        float bestFitness = fitnessFunction.calculateFitness(parents.get(1));
        System.out.println("Generation " + currentGeneration + " best fitness: " + bestFitness);

        // cross parents together, mutate some genes, create new children
        for (int i = 0; i < desiredPopSize; i++) {
            Car child = crossParents(parents.get(0), parents.get(1));
            Car mutatedChild = mutateChild(child);
            children.add(mutatedChild);
        }

        currentGeneration++;
        population = children;
        return children;
    }

    private Car crossParents(Car p1, Car p2) {
        Car child = new Car(new Position(startX, startY), p1.getGenome());
        float[] crossedGenome = new float[p1.getGenome().length];
        for (int i = 0; i < crossedGenome.length; i++) {
            if (Math.random() <= uniformRate) {
                crossedGenome[i] = p2.getGenome()[i];
            } else {
                crossedGenome[i] = p1.getGenome()[i];
            }
        }
        child.setGenome(crossedGenome);
        return child;
    }

    private Car mutateChild(Car c1) {
        float[] mutatedGenome = new float[c1.getGenome().length];
        for (int i = 0; i < c1.getGenome().length; i++) {
            if (Math.random() <= mutationRate) {
                mutatedGenome[i] = Math.random() >= 0.5 ? (float)Math.random() : (float)-Math.random();
            } else {
                mutatedGenome[i] = c1.getGenome()[i];
            }
        }

        return new Car(c1.getPosition(), mutatedGenome);
    }

    private List<Car> getBestParents(List<Car> population) {
        population.sort(Comparator.comparing(c -> fitnessFunction.calculateFitness(c)));

        return population.subList(population.size() - 2, population.size());
    }

    public List<Car> getPopulation() {
        return population;
    }
}
