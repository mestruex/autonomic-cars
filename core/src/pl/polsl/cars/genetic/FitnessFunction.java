package pl.polsl.cars.genetic;

import pl.polsl.cars.model.Car;
import pl.polsl.cars.model.Position;

import java.util.List;

public class FitnessFunction {

    private List<Position> checkpoints;
    private final float maxFitness;

    public FitnessFunction(List<Position> checkpoints) {
        this.checkpoints = checkpoints;

        float fitness = 0;
        for (int i = 0; i < checkpoints.size() - 1; i++) {
            fitness += calculateDistance(checkpoints.get(i), checkpoints.get(i + 1));
        }

        this.maxFitness = fitness;
    }

    public Float calculateFitness(Car car) {
        int currentCheckpoint = car.getPassedCheckpoints();
        float fitness = 0;

        for (int i = 0; i < currentCheckpoint - 1; i++) {
            fitness += calculateDistance(checkpoints.get(i), checkpoints.get(i + 1));
        }

        if (currentCheckpoint == checkpoints.size()) {
            if (!car.isGenomePrinted()) {
                System.out.println("Finish line reached, printing genome");
                car.printGenome();
                car.setGenomePrinted(true);
            }
            return maxFitness;
        }

        fitness += (calculateDistance(checkpoints.get(currentCheckpoint - 1), checkpoints.get(currentCheckpoint)) -
            calculateDistance(car.getPosition(), checkpoints.get(currentCheckpoint)));

        return fitness;
    }

    private float calculateDistance(Position pos1, Position pos2) {
        return (float)Math.sqrt(Math.pow(pos2.getX() - pos1.getX(), 2) + Math.pow(pos2.getY() - pos1.getY(), 2));
    }

    public float getMaxFitness() {
        return maxFitness;
    }
}
